module sixbell/traefik/shardmiddleware

go 1.17

require (
	github.com/jkeys089/jserial v1.0.1
	github.com/licaonfee/redigo v1.9.1
)

require github.com/pkg/errors v0.8.1 // indirect
