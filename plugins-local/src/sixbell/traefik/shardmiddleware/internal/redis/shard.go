package redis

import (
	"context"
	"fmt"
	"os"

	"github.com/licaonfee/redigo/redis"
)

func NewRedisShardCli() (redis.Conn, error) {

	fmt.Println("setting Up new Database Connection")
	redisHost := os.Getenv("REDIS_SHARD_HOSTNAME")
	redisPort := os.Getenv("REDIS_SHARD_PORT")
	//redisPass := os.Getenv("REDIS_TENANT_PASS")
	//redisDB,err := strconv.Atoi(os.Getenv("REDIS_SHARD_DB"))

	c, err := redis.DialContext(context.Background(), "tcp", redisHost+":"+redisPort)
	if err != nil {
		fmt.Println(err)
		return nil, nil
	}
	//defer c.Close()

	return c, nil
}
