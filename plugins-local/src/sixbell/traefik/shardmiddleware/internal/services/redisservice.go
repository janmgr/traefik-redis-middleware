package services

import (
	"encoding/json"
	"fmt"
	"github.com/jkeys089/jserial"
	"log"
	"sixbell/traefik/shardmiddleware/internal/storage"
)

type RedisService struct {
	uaasRepository *storage.UaasRepository
	tenantRepository *storage.ShardRepository
}

type AuthResponse struct{
	Authentications []Authentication
}

type Authentication struct {
	Authenticated bool `json:"authenticated"`
	UserAuthentication UserAuthentication `json:"userAuthentication"`
}

type UserAuthentication struct {
	Principal Principal `json:"principal"`
}

type Principal struct{
	ClientId int `json:"_clientId"`
}



func NewRedisService(uaasRepository *storage.UaasRepository, tenantRepository *storage.ShardRepository) *RedisService {
	return &RedisService{
		uaasRepository: uaasRepository,
		tenantRepository: tenantRepository,
	}

}

func (r *RedisService) GetShardId(token string) (shardId string, err error){


	infoToken, err := r.uaasRepository.GetInfoToken(token)
	if err!=nil {
		log.Printf("%+v",err)
	}

	deserializedInfoToken, err := r.deserializeInfoToken(infoToken)


	var authResponse AuthResponse
	err = json.Unmarshal([]byte(deserializedInfoToken), &authResponse.Authentications)

	if err != nil {
		fmt.Println(err)
	}

	var clientId = authResponse.Authentications[0].UserAuthentication.Principal.ClientId
	fmt.Printf("%#v", clientId)

	shardId, err = r.tenantRepository.GetShardByClientId(clientId)

	return shardId,nil
}

func (r *RedisService) deserializeInfoToken(serializedInfoToken string) (string, error) {

	objects, err := jserial.ParseSerializedObjectMinimal([]byte(serializedInfoToken))
	if err != nil {
		log.Fatalf("%+v", err)
		return "", err
	}

	jsonStr, err := json.MarshalIndent(objects, "", "    ")
	if err != nil {
		log.Fatalf("%+v", err)
		return "", err
	}

	fmt.Println(string(jsonStr))

	return  string(jsonStr), nil

}


