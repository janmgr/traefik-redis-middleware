package shardmiddleware

import (
	"context"
	"fmt"
	"net/http"
	"sixbell/traefik/shardmiddleware/internal/redis"
	"sixbell/traefik/shardmiddleware/internal/services"
	"sixbell/traefik/shardmiddleware/internal/storage"
)

const defaultShardHeader = "X-Shard-ID"

const defaultRedisUassHost = "localhost"
const defaultRedisUassPort = "6379"
const defaultRedisUassPass = ""
const defaultRedisUassDB = 0

const defaultRedisShardHost = "localhost"
const defaultRedisShardPort = "6379"
const defaultRedisShardPass = ""
const defaultRedisShardDB = 0

// Config holds configuration to passed to the plugin
type Config struct{
	ShardHeaderName string
	RedisUassHost string
	RedisUassPort string
	RedisUassPass string
	RedisUassDB int
	RedisShardHost string
	RedisShardPort string
	RedisShardPass string
	RedisShardDB int
}

// CreateConfig populates the Config data object
func CreateConfig() * Config{
	return &Config{
		ShardHeaderName: defaultShardHeader,
		RedisUassHost: defaultRedisUassHost,
		RedisUassPort: defaultRedisUassPort,
		RedisUassPass: defaultRedisUassPass,
		RedisUassDB: defaultRedisUassDB,
		RedisShardHost: defaultRedisShardHost,
		RedisShardPort: defaultRedisShardPort,
		RedisShardPass: defaultRedisShardPass,
		RedisShardDB: defaultRedisShardDB,

	}
}

type ShardMiddleware struct {
	next 			http.Handler
	shardHeaderName	string
	name 			string
	redisService    services.RedisService
}

// New instantiates and returns the required components used to handle  a HTTP request
func New(ctx context.Context, next http.Handler, config *Config, name string) (http.Handler, error){

	fmt.Println("Setting Up Our App")

	if len(config.ShardHeaderName) == 0 {
   		return nil, fmt.Errorf("Empty Shard Header Name")
   }

   redisUaasCli, err := redis.NewRedisUaasCli()
   if err != nil {
   	  fmt.Println("Error creating redis uaas cli")
   }

   redisShardCli, err := redis.NewRedisShardCli()
   if err != nil {
		fmt.Println("Error creating redis shard cli")
   }

   redisUaasRepository := storage.NewUaasRepository(redisUaasCli)
   redisShardRepository := storage.NewShardRepository(redisShardCli)

   redisService := services.NewRedisService(redisUaasRepository,redisShardRepository)
   if err != nil {
   		fmt.Println("Error creating redis service")
   }

	return &ShardMiddleware{
  	next: 	next,
  	shardHeaderName: config.ShardHeaderName,
  	name: 	name,
  	redisService: *redisService,
  },nil
}

func (s *ShardMiddleware) ServeHTTP(rw http.ResponseWriter, req *http.Request){
	token := req.Header.Get("token")

	shard, err := s.redisService.GetShardId(token)
    if err != nil {
    	fmt.Println("Error at obtain shard")
	}

	//A quien le pregunto por el shard---->   client_id----> token
	// header injection to backend service
	req.Header.Set(s.shardHeaderName, shard)
	// header injection to the client response
	rw.Header().Add(s.shardHeaderName ,shard)

	s.next.ServeHTTP(rw,req)
	//rw.Write([]byte("Hello World"))


}

