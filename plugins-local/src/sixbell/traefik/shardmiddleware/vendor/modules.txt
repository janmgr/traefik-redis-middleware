# github.com/jkeys089/jserial v1.0.1
## explicit
github.com/jkeys089/jserial
# github.com/licaonfee/redigo v1.9.1
## explicit; go 1.17
github.com/licaonfee/redigo/redis
# github.com/pkg/errors v0.8.1
## explicit
github.com/pkg/errors
